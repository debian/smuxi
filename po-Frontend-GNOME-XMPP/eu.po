# Basque translation for smuxi.
# Copyright (C) 2022 smuxi's COPYRIGHT HOLDER
# This file is distributed under the same license as the smuxi package.
# Asier Sarasua Garmendia <asiersarasua@ni.eus>, 2022.
#
msgid ""
msgstr "Project-Id-Version: smuxi master\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/smuxi/issues\n"
"POT-Creation-Date: 2022-04-04 16:19+0000\n"
"PO-Revision-Date: 2022-04-04 18:08+0000\n"
"Last-Translator: Asier Sarasua Garmendia <asiersarasua@ni.eus>\n"
"Language-Team: Basque <librezale@librezale.eus>\n"
"Language: eu\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../src/Frontend-GNOME-XMPP/XmppGroupChatView.cs:247
msgid "Query"
msgstr "Kontsulta"

#: ../src/Frontend-GNOME-XMPP/XmppGroupChatView.cs:255
#: ../src/Frontend-GNOME-XMPP/XmppPersonChatView.cs:215
msgid "Whois"
msgstr "Whois"

#: ../src/Frontend-GNOME-XMPP/XmppGroupChatView.cs:261
#: ../src/Frontend-GNOME-XMPP/XmppPersonChatView.cs:221
msgid "Add To Contacts"
msgstr "Gehitu kontaktuei"

#: ../src/Frontend-GNOME-XMPP/XmppGroupChatView.cs:267
#: ../src/Frontend-GNOME-XMPP/XmppPersonChatView.cs:227
msgid "Invite to"
msgstr "Gonbidatu honi:"

#: ../src/Frontend-GNOME-XMPP/XmppGroupChatView.cs:285
msgid "Rename"
msgstr "Aldatu izena"

#: ../src/Frontend-GNOME-XMPP/XmppGroupChatView.cs:297
msgid "Remove"
msgstr "Kendu"
