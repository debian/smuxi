-----BEGIN PGP PUBLIC KEY BLOCK-----
Comment: A371 2C8E 4CE4 49B9 F28C  563F 33A4 29E6 2D90 7822
Comment: Mirco Bauer (YubiKey II) <meebey@meebey.net>

xsFNBGE0UuQBEADuDKwxObXewp9AJXV0bBWAS5fN1f7hTQWvKVtU3QHhQoVR9n7A
e8Pm+VvizGE+BY4oAH6lnCjRhYmiMLET0eRFIZ8LWvyVqdjg4IAoznKpRsao0KTK
BjkllKYpIarcpA1QZZyLizsePDIG0sA/efrndqh+XhitqMnf0eaWUDHmE0dswQ/F
zl4KFAAvLZnjNaFJAcphrMiATT4kyzHbMQTvXTdU3VIsvyetqWaR2zDxzz7gOOKY
L/NBKZ+YIEQf6hyFke/nrXEfBWB9Y3vP9MzWIKYfbhHpuhNkNJPnjEqmPnO9ag3c
8Hkzh74qlxziR2i5N5NDtJwzM5IheGkb8Yof16DEsYjJ75Jb/64mY99LV73c4Rhx
nBlm+4KtPJ2cq5YKeI017+/E9aJvm8rNVK4whDnaiV7uWO/RUueCpcNlSXz7XwQG
O3DpO11BtDdAfEhjRzRIJnJxP+eUgaIMVRFuj4FpIuT6XBm04S/bpLpsiKBk5sTV
VOvMs9EvpcA8+IP14/yoQofBFl+stUbw+vhTvq1G9DGLltRgSFI85g6Oan46jbBW
piyGJBYy6TKuCQozPLR/Qm5NjFmQNODb6hhoWwm0P3su/xWmV5fIYy7zZFrsgLaM
+miP7t9Y+GjepfbM14zenoYrxj1RjOsVh2otupfI24Sdv77dlZXe4TjsRQARAQAB
zSxNaXJjbyBCYXVlciAoWXViaUtleSBJSSkgPG1lZWJleUBtZWViZXkubmV0PsLB
jgQTAQgAOBYhBKNxLI5M5Em58oxWPzOkKeYtkHgiBQJhNFLkAhsDBQsJCAcDBRUK
CQgLBRYCAwEAAh4BAheAAAoJEDOkKeYtkHgic0kP/jih7oxsH3wayD7xjw5+uBNY
pTFbwyeo//l80sppA+IsT8f+FpTgHD4zXhCvoxFYvE7PyhDCilvWEqL20cCh4cWt
lL+UTB8n6VnOwgWfErE4FcrxKTmJbIlcHv+tbMS1G1gucFjva2irZcGwfuAh4HC4
MMaY5hWHn8d6BxGmEg9rHNfc8xpZkGx8ttFMwA88LgHd5n0q4vDpyQ+FbiPO+yRd
2MVUGsi3vZlQQ6cCz/x6OO9km84dq0WXtUS1XXgFLzyn4eRYJbBAkSollv6D2la6
UEvm2Pz/igLlhMcGX5KSr4m1D/JUDvUfli273c/cBzyuKaGXZFBc0Au+DQBEh4k2
bZfQqiZpvDyYMAbyaDeRB3igR/rVThh3qDP7U6q5PWjWcLyR9Jl3VX4OyhWd63fd
YYof+X6CHFC49ZCelmy+GUmATI/Fs/GxdwASCoEeQtUEznA7dnGXghMaPLnPL/cI
RhoJtPgdRmaKwsTOGhYYFzrcdd5OK7Fil23XjsuB7K6IZ8pnMFZg9OYz9853B8ch
PcsAXh2+Vg6uYVXeiV+pB4f4QjLMA8uVJkNKHwtcwOS0RaDcVmn9wm8r1YMXu6P6
0RJBCeWDekkC/i8PwwPkco+oyK+NRCB7pto+RpWbUxkGeZ3uQgANy3dU36OtEnzW
1zJhcpgIU+qCxp3bYysbzsFNBGE0UuQBEADBUeIWTdBpKMC2m0xfuavQYZAi2Ie/
DruVXQ6A5v75j+1a7s/puKx+nhuYEYnwdQ3q3oXUFrEwiq4TLdcE0REZAqTFa9oX
g5P5jhyYPWKy+XIQOOtYHFo993k8Pf+nHh56k5lf4YNyMP3chTkgKY2W4H59BCo0
MHkgEWt9wzRKHAzQNcBDKGTbwwhy3xBXFnQJakjf/E0QqVNO43Edi8MuMkC8XPMz
DcEGBAjpDo9I2rg6VhuDUqKIzlkRWA0C9X0xxawvvPWgYJmnYEolfHiIhRMtC2Yo
P0h1XXwdxtLRGeo280VrJacYJD4gN2PirN0dFya/X2UeBb4i2yZG0fELJ+i7iATw
DXyygGJ0SauS9l+gssMqHRidzytopwtaHCN3Srwl4pXqaRJqGl5KLESEpXMQ+7km
i79Ypc9NC2oZgWdoHrrY1aQoXvRrwy8ufXdKK1Ojo72F5I56gUONuEzuKR3Ps09r
zlKP/xAV9QB+sqI5BWiHzkwJvduAE+6NLnpZuP8otPNb7xNKNQHaRDFx9D9q/y0s
wkJaz5BQYd3RyJUf2bngbA4qDwdUKNlXsSLqJX/3oJ7KggNc+OeHKGLNZ/ny3jzD
PAYsCaNF8X0aemhLeywNnZ4SThu2jUwevnE3PzrvIkWRvgWFWDOP2CD72wxK0nCu
bJAFGfJFhsA15QARAQABwsF2BBgBCAAgFiEEo3EsjkzkSbnyjFY/M6Qp5i2QeCIF
AmE0UuQCGyAACgkQM6Qp5i2QeCJiZA/7BFC367XbJVXQT02GNYeti6FTuhWJCH3V
M+mD1Ig0BnYNdOlZ5lq66eglcmVK/jnsijw9J2qrXT/Dit3M6JKZnrs4k4TRueGF
SE+lWmFwXcwIlP7d8by035nAgv0Qx8GhRv4Zw0Ol7wQ+zgnPBPo4oSyKWaOEUiyT
Wr4y3/z9nd6JuXSaWcrX8prrFI4ceqY+VPMcrTL/nIcz828OmWo+fBzyu/bBAzM4
Rv5SvAXLd9nqQcyV4+SgD0zpuio5ol7SzC9w+Ybj8VMPutn6VtiDcRWqeiMmicFU
Y8cBsE7BwGSTGOkqihoyMYowJhIXtW1UzBjgZfvBuJxi87rCDqeY7CCZY5NO+Ok7
Q9/mtlausl2xENk+n3lQZrUwpeUCOTA/andgk6O7BRT2+5GDXIoWzbtDlPYpE7kU
8XHTbvAqrsslaYO5go0NOUC4qnWiWoZ/PWcYJo2I+yyXLi1R5zweaNqdilDncTj7
uZfg9oS+DKLaaihzLsakKlOxrOBlJjbmq86CC4e8M+sNV0mPqqlE0kudnos/HOOK
rRAjpH6Na5yiBpJ7mcQ6/JF5k9uHP/X1zowLXQtT7gmApqlTODVnNBFWh7/ihUys
du/31Q6MzuQfzd3tjJwV0hLgF1wq+qrBfAJyrBGOZcScxIy9eUBBjRkEKII2Lsa7
f/ntlzO0fR7OwU0EYTRS5AEQAO0SxtJnEe5q4nFW2RR/yzFtjxe3Y8sngnDcEfuj
xUUDysNEAbGZrsp1KeHXschNIIBMWvTIdJfQOz5YIhoNIe8UM42PKtVG2dGcfYjT
FFZQkuCZUgl5qBNPWZDrV0XjQZ5Jup/pSvFRcO37I798esS3cU4xjRlcc8Je2ARy
orFrfvOhjNbv24g1DDHF8RNnM84fhn/iJxA3vfdvhFXRtAICiee0+Deo8HBJPHV+
5io3t1gBg6sNmxnYDoLDCWkIHFBCthvSNIt6ulT8QFIc8ja0VknwoE4wSpDNXCMD
SjqjH0RJx3P90ocANlCQ9YAXGWS6O4y9L+ONQ7eM6yLIGtHt8uFR7zjh1N/rDm+J
p8kIzlMYMGM+0S/LlcAtB/N2jU0vVg9aTlayxBMVVZiYXweXO0ZqBhlkWZDAD/58
xb1322DIQmmilRpjBao5g1BGxpXgON/pnW/ov+kGbvOeAtOS27IbIOLsheKGNmV5
Fl1KMV9IaHfvfY1elKFO3VlJPOXVwI/mdgqlYa7ApSZSg83qelEXxsn5tGnSdYRE
U548N8qeiqyV3l9znEIN0BBYB/cAE+dIKptyYPvQ0fXQL/g85qBmi0Z8L8W2TuBX
r8Udm4tqk6HxhRI9edbZULfmxoFIJYDB5frAjbHGhxfEcvQTWD18jV1CaioDH+dA
W7TBABEBAAHCwXYEGAEIACAWIQSjcSyOTORJufKMVj8zpCnmLZB4IgUCYTRS5AIb
DAAKCRAzpCnmLZB4IggGEADVSyg09eB+8iE5P6ZbI+61IyPYdNCbBPAnBAURi5sW
MrWVnVxpIZetAY9VFiedlwKVI4uQWSvayxEQKIWC9MIYbQGqmo5pPWp6zGgg8RtP
nKWa8/u957NtvG76yj2pwnbwssh+VX0FHBEXitRhoJ2XUpdvvZpPglNjkokmwqf+
ZVtuGM+5nwA6xNm6q6Uil2iYNyrEX0z3HgcKhWvyEYjmrvo2ugNXqDchLO0CrSYz
RJx71nxCTLSMJe9BGjiNyKnmplZiC2b6DcUL9fXkYX9etLn6qbbXjY+u1gcKR0jU
tOhKgriuMvpBGABTm7GSuJ6PgDLJ6VcNJ40w1lH+UezAnVSMNqIvpUwE0Wklif5O
0dLR7+kWYHS8hiyx4PvYQm7SnoUK/6qtGhwucizotXKxVKd8JB/bOH2HD6rEPput
CIMz4ZKeYXyx+uUPR613ogtbXScliB0plEaPf5C3uMQej18lgCbhjlI+nV98bdFz
kuzmglDe5vKtcHYXVLX4wMW7eOGRBpjcRW5C0OKiAT1FX1H7c+dEeXrKKoosZDGR
cXKz0dANX52w/hmHj+UgZCCVlwEcIgE5fm5vr2nEHCCx1v+tU0/tkIrIOg2C1U46
eZvqUt/Bi96dfh7rAMkiOO87v6X5dUmQQqJkjIIprdcKENjwPuNXXn4N8zrwv6nM
ww==
=5Bbs
-----END PGP PUBLIC KEY BLOCK-----
